#!/bin/sh

LOG_IP=docker
LOG_PORT=13006
PAYMENT_IP=docker
PAYMENT_PORT=13002
DELIVERY_IP=docker
DELIVERY_PORT=13003
MACHINE_IP=docker
MACHINE_PORT=13005
ORDER_IP=docker
ORDER_PORT=13004

wget http://$LOG_IP:$LOG_PORT/health -O /dev/null --retry-connrefused
wget http://$PAYMENT_IP:$PAYMENT_PORT/health -O /dev/null --retry-connrefused
wget http://$DELIVERY_IP:$DELIVERY_PORT/health -O /dev/null --retry-connrefused
wget http://$MACHINE_IP:$MACHINE_PORT/health -O /dev/null --retry-connrefused
wget http://$ORDER_IP:$ORDER_PORT/health -O /dev/null --retry-connrefused

