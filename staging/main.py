import unittest
from tests.test_order_smoke import OrderTestCase
from tests.test_payment_smoke import PaymentTestCase
from tests.test_client_smoke import ClientTestCase

def suite():
    suite = unittest.TestSuite()
    suite.addTest(OrderTestCase('test_order_creation'))
    suite.addTest(PaymentTestCase('test_payment_creation'))
    suite.addTest(ClientTestCase('test_client_delete'))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
