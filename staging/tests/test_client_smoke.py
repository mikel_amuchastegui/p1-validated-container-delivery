import unittest
import os
import json
import requests
import random
from time import sleep

class ClientTestCase(unittest.TestCase):

    host = os.getenv("HAPROXY_IP")
    port = os.getenv("HAPROXY_PORT")
    commit_sha = os.getenv("CI_COMMIT_SHORT_SHA", str(random.randrange(1, 1000000, 1)))

    def setUp(self):
        # Create a client based on commit SHA
        path = "/client"
        url = "http://"+self.host+":"+self.port+path
        data = {"username": "userclient"+str(self.commit_sha)}
        requests.post(url, data=json.dumps(data), headers={"Content-type": "application/json"})
       
        # Create a payment for the client created before
        path = "/payment"
        url = "http://"+self.host+":"+self.port+path
        data = {"user_id": "userclient"+str(self.commit_sha), "balance": 1000}
        requests.post(url, data=json.dumps(data), headers={"Content-type": "application/json"})

    def test_client_delete(self):
        # Delete user
        path = "/client"
        url = "http://" + self.host + ":" + self.port + path + "/userclient" + str(self.commit_sha)
        resp = requests.delete(url, headers={"Content-type": "application/json"})
        data = resp.json()
        assert resp.status_code == 200

        # Check payment deleted
        path = "/payment"
        url = "http://" + self.host + ":" + self.port + path + "/userclient" + str(self.commit_sha)
        resp = requests.get(url, headers={"Content-type": "application/json"})
        data = resp.json()
        assert resp.status_code == 404

