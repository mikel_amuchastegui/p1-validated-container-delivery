import unittest
import os
import json
import requests
import random

class PaymentTestCase(unittest.TestCase):

    host = os.getenv("HAPROXY_IP")
    port = os.getenv("HAPROXY_PORT")
    commit_sha = os.getenv("CI_COMMIT_SHORT_SHA", str(random.randrange(1, 1000000, 1)))

    def setUp(self):
        # Create a client based on commit SHA
        path = "/client"
        url = "http://" + self.host + ":" + self.port+path
        data = {"username": "userpayment" + str(self.commit_sha)}
        requests.post(url, data=json.dumps(data), headers={"Content-type": "application/json"})

    def tearDown(self):
        path = "/client"
        url = "http://" + self.host + ":" + self.port + path + "/userpayment" + str(self.commit_sha)
        resp = requests.delete(url, headers={"Content-type": "application/json"})

    def test_payment_creation(self):
        # Create a payment for the client created before
        path = "/payment"
        url = "http://" + self.host + ":" + self.port + path
        data = {"user_id": "userpayment"+str(self.commit_sha), "balance": 1000}
        resp = requests.post(url, data=json.dumps(data), headers={"Content-type": "application/json"})
        data = resp.json()
        assert resp.status_code == 200
        assert data["balance"] == 1000

        # Check payment has been done correctly
        path = "/payment"
        url = "http://" + self.host + ":" + self.port + path + "/userpayment" + self.commit_sha
        resp = requests.get(url, headers={"Content-type": "application/json"})
        data = resp.json()
        assert resp.status_code == 200
        assert data["balance"] == 1000

        # Check logging
        path = "/log"
        url = "http://" + self.host + ":" + self.port + path
        resp = requests.get(url, headers={"Content-type": "application/json"})
        data = resp.json()
        assert resp.status_code == 200
        assert "POST new Payment created:" in data[-1]["data"]

