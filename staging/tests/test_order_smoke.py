import unittest
import os
import json
import requests
import random
from time import sleep

class OrderTestCase(unittest.TestCase):

    host = os.getenv("HAPROXY_IP")
    port = os.getenv("HAPROXY_PORT")
    commit_sha = os.getenv("CI_COMMIT_SHORT_SHA", str(random.randrange(1, 1000000, 1)))

    def setUp(self):
        # Create a client based on commit SHA
        path = "/client"
        url = "http://"+self.host+":"+self.port+path
        data = {"username": "userorder"+str(self.commit_sha)}
        requests.post(url, data=json.dumps(data), headers={"Content-type": "application/json"})
       
        # Create a payment for the client created before
        path = "/payment"
        url = "http://"+self.host+":"+self.port+path
        data = {"user_id": "userorder"+str(self.commit_sha), "balance": 1000}
        requests.post(url, data=json.dumps(data), headers={"Content-type": "application/json"})

    def tearDown(self):
        path = "/client"
        url = "http://" + self.host + ":" + self.port + path + "/userorder" + str(self.commit_sha)
        resp = requests.delete(url, headers={"Content-type": "application/json"})

    def test_order_creation(self):
        # Perform order
        path = "/order"
        url = "http://" + self.host + ":" + self.port+path
        data = {"client_id": "userorder"+self.commit_sha, "number_of_pieces": 10, "address": "48"}
        resp = requests.post(
            url,
            data=json.dumps(data),
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        order_id = data["id"]
        assert resp.status_code == 200
        assert data["status"] == "Created"

        sleep(5)
        # Check payment
        path = "/payment"
        url = "http://" + self.host + ":" + self.port+path + "/userorder" + self.commit_sha
        resp = requests.get(
            url,
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        assert resp.status_code == 200
        assert data["balance"] == 1000 - (10*10)

        # Check order has delivery assigned
        path = "/order"
        url = "http://" + self.host + ":" + self.port+path + "/"+str(order_id)
        resp = requests.get(
            url,
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        assert resp.status_code == 200
        assert data["delivery_id"] is not None
        delivery_id = data["delivery_id"]
        
        # Check delivery is correctly created
        path = "/delivery"
        url = "http://" + self.host + ":" + self.port+path + "/" + str(delivery_id)
        resp = requests.get(
            url,
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        assert resp.status_code == 200
        assert data["order_id"] == order_id
        assert data["status"] == "Created"

        # Wait for pieces manufacturing
        sleep(60)
        
        # Check number of pieces manufactured is Ok
        path = "/order"
        url = "http://" + self.host + ":" + self.port+path + "/"+str(order_id)
        resp = requests.get(
            url,
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        assert resp.status_code == 200
        assert data["pieces_manufactured"] == 10
        
        # Check delivery is finished
        path = "/delivery"
        url = "http://" + self.host + ":" + self.port+path + "/" + str(delivery_id)
        resp = requests.get(
            url,
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        assert resp.status_code == 200
        assert data["order_id"] == order_id
        assert data["status"] == "Finished"
        
        # Check order status is delivered
        path = "/order"
        url = "http://" + self.host + ":" + self.port+path + "/"+str(order_id)
        resp = requests.get(
            url,
            headers={"Content-type": "application/json"}
        )
        data = resp.json()
        assert resp.status_code == 200
        assert data["status"] == "Delivered"

