# Export some environment variables in order to pull the images correctly from the registry.
# These values may be changed for other projects.
export CI_REGISTRY=registry.gitlab.com
export CI_PROJECT_NAMESPACE=mikel_amuchastegui
export CI_PROJECT_NAME=p1-validated-container-delivery

# Clone the repository.
# As the code is mounted in a volume we need it.
git clone https://gitlab.com/mikel_amuchastegui/p1-validated-container-delivery.git
cd p1-validated-container-delivery

# Log in to the registry.
# This command will ask for GitLab username and password.
docker login $CI_REGISTRY
# Start services.
# We are up :)
docker-compose -f docker-compose.production.yml up -d
