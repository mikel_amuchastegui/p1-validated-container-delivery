# P1-doc
En este documento se recogen la definición y evidencias de la **Práctica 1º** denominada **Validated Container Delivery** de la asignatura **Integración y Despliegue Continuo** del master [Análisis de datos, Ciberseguridad y Computación en la nube](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube)

## Test de Integración y Unitarios

Se han desarrollado tanto tests de integración como test unitarios de cada microservicio. Se han testeado todos los métodos que están de cara al público exterior, esto es, todos los métodos accesibles desde la API.

En caso de los tests de integración, el propio método no se ha aislado de las funciones al exterior, es decir, el envío de mensajes por RabbitMQ, el logging de mensajes, o la propia Saga en caso del microservicio Order siguen funcionando. La siguiente imagen muestra un ejemplo de un test de integración en el microservicio Order.

![](img/integration_test.png)

Por otro lado, la siguiente imagen muestra el mismo test anterior pero de forma unitaria, donde tanto el acceso a la base de datos como la SAGA y las comunicaciones con RabbitMQ han sido mockeadas.

En caso de la SAGA y las comunicaciones a la escucha (consumidoras), simplemente han sido deshabilitadas mediante la librería monkeypatch. En cambio, en caso las comunicaciones de RabbitMQ de envío de mensajes al exterior (productoras), la función de envío de datos ha sido mockeada, sustituyendola por una función mock que no realiza ninguna acción. Se ha decidido llevar esta estrategia a cabo, porque, mediante la deshabilitación de los comunicaciones a la escucha se ha querido evitar la puesta en marcha del hilo que se mantiene a la espera de esas comunicaciones.

![](img/unit_test.png)

Finalmente, cabe destacar como se puede ver en ambas imágenes como se ha hecho uso en ambos casos de fixtures para la inicialización de la aplicación, la incialización y destrucción de la base de datos y funciones de ayuda para insertas nuevos objetos en la base de datos. La siguiente imagen, muestra la configuración de los tests en caso del microservicio Order que ayuda a entender esto de forma adecuada.

![](img/conftest.png)

## Smoke Test Suite

El Test Suite desarrollado se compone de tres Smoke Tests. Primeramente, se desarrollaron dos de ellos: test de creación de un Order y test de creación de un Cliente con la comprobación adecuada del Logging.

Aún y todo, con el desarrollo de las nuevas funcionalidades se ha desarrollado un tercer Smoke Test que comprueba el proceso de elimicación de un cliente. Las siguientes secciones explican brevemente aunque de forma un poco más detallada cada uno de los test, y finalmente, se incluye una captura del resultado de los Smoke Tests en la fase de staging del despliegue.

### Smoke Test Order

El test de creación de un order sigue el siguiente proceso:

1. En el setup del test se crean tanto un Cliente como un Payment asociado a ese cliente para poder crear así un order.
2. En el propio test se comprueban las siguientes acciones: se verificar que el Order se ha creado de forma correcta, se verifica también que la cartera del usuario se actualice restándole el importe adecuado, se comprueba la asignación de un Delivery, y finalmente, se realizan varias comprobaciones dentro del proceso de actualización tanto del estado de un Delivery como del propio Order, hasta que ambos finalizan de forma adecuada.
3. Finalmente, en el teardown del test se hace uso de la nueva funcionalidad desarrollada para eliminar el Cliente y posterior Payment asociado creados en el setup.

### Smoke Test Payment

El Smoke Test de Payment junto con el Logging sigue el siguiente proceso:

1. En el setup se crean un Cliente para poder asociarle un Payment en el siguiente paso.
2. El propio test crea un Payment, comprueba que se ha realizado de forma correcta y una vez hecha esta comprobación, verifica que el último mensaje loggeado en la aplicación sea correspondiente a esta acción y este loggeado de forma adecuada.
3. Finalmente, en el teardown del test se hace uso de la nueva funcionalidad desarrollada para eliminar el Cliente creado en el setup y su Payment asociado creado en el test.

### Smoke Test Client

El último Smoke Test comprobando el proceso de eliminación de un cliente sigue el siguiente proceso:

1. En el setup del test se crean tanto un Cliente como un Payment asociado a ese cliente.
2. En el propio test se elimina ese Cliente y se comprueba que se ha eliminado de forma correcta. Una vez realizado esto, se comprueba que el Payment de ese Cliente también se haya eliminado de forma adecuada de la aplicación.
3. Finalmente, en el teardown del test se hace uso de la nueva funcionalidad desarrollada para eliminar el Cliente y posterior Payment asociado creados en el setup.

La siguiente imagen muestra el resultado de los Smoke Tests en la fase **staging** del Pipeline de CI de GitLab. En la imagen se puede observar como los tres tests ejecutados son satisfactorios, por tanto, podríamos pasar a la fase de deploy. 

![](img/smoke.png)

## Nueva funcionalidad: eliminación de un usuario y un payment

Al comienzo del desarrollo, se planteó desarollar una nueva funcionalidad de eliminación de un usuario, pero al ser una funcionalidad sencilla, con objetivo de darle mayor robustez y sentido a la aplicación se ha desarrollado también la funcionalidad de eliminación de un Payment. De esta forma, se ha conseguido que al eliminar un usuario se elimine también su Payment asociado y su dinero desaparezca de la aplicación.

Para dejar constancia y evidencia de la metodología TDD (Test Driven Development) seguida se ha optado por realizar commits por cada test-funcionalidad implementada, para poder observar de forma visual mediante el estado del pipeline los cambios llevados a cabo, ya que, el pipeline comenzará fallando al implementar el test sin la funcionalidad y terminará de forma adecuada al implementar la funcionalidad. 

Esto se puede observar en el historial del branch "delete-client" donde se ha ido implementado esta funcionalidad. En la siguiente imagen se muestra una captura de esa misma historia.

![](img/TDD.png)

Como se puede observar en la imagen, el pipeline falla cuando se implementa el test y pasa cuando se implementa la funcionalidad correspondiente a este test. Para más información se puede acceder también a los mensajes de los commit en esa misma historia.

En caso de la funcionalidad, "delete-payment" el desarrollo y la metodología seguidas han sido equivalentes, pero en este caso, no se ha ido haciendo un commit por cada test-funcionalidad implementada, sin embargo, el mismo proceso se ha llevado a cabo de forma local para ahorrar el tiempo de espera de ejecución del pipeline y mantenerlo además más tiempo en verde.

## Pipeline CI

En este caso, se han desarrollado dos distintos pipelines de CI (.gitlab-ci.yml y .gitlab-ci-jobs.yml) siendo el segundo de ellos el que se ejecuta en la actualidad.

Ambos pipelines cumplen con la misma función. Ambos están divididos en cuatro fases o stages: build, test, delivery y staging. Para cada uno de esos stages (excepto en la fase delivery, donde únicamente se taguean las imágenes de manera adecuada) se ha desarrollado un docker-compose distinto. 

El objetivo de estos distintos docker-compose es poder cumplir con las necesidades de cada fase de forma adecuda, esto es, en la fase build, el docker-compose debe incluir una fase build de las imágenes que en las demás fases no se debe incluir y, por tanto, el proceso y el propio docker-compose deben ser ligeramente distintos. Este mismo problema ocurre con la configuración del entorno de base de datos (testing, production o development) y el tag de las imágenes a obtener en cada stage, por tanto, para facilitar el manejo de estos cambios se ha optado por hacer uso de distintos docker-compose.yml

La única diferencia que contiene el pipeline .gitlab-ci-jobs.yml frente a .gitlab-ci.yml es el modo de ejecución del trabajo en el stage test. En este caso, se han creado distintos trabajos (uno por cada microservicio) para un mismo stage test, de esta manera, se permite la ejecución paralelizada de los tests de cada microservicio y es posible obtener también una media aritmética del coverage de todos los microservicios, lo que es de gran utilidad, ya que, en caso del pipeline .gitlab-ci.yml solo era posible mostrar el coverage de la última ejecución de los tests.

En la siguiente imagen se muestran los contenedores generados y almacenados en el Container Registry de GitLab.

![](img/containers.png)

Junto con los containers almacenados en el Container Registry, se ha configurado una política de expiración de los contenedores con tag de test, para mantener así, un control sobre el espacio utilizado. La siguiente imagen muestra la configuración realizada.

![](img/tag_policy.png)

## Deploy Manual

Para el deploy manual se han creado dos scripts: setup.sh y deploy.sh. 

Ambos son scripts que se ejecutan manualmente, una vez se tiene acceso a una máquina EC2 de AWS. El script setup.sh configura la máquina de forma adecuada, instalando Docker, Docker Compose y Git, mientras que, el script deploy.sh se encarga de hacer el deploy mediante los contenedores del Container Registry de GitLab.

Las siguientes imagenes muestran como se ejecuta el script deploy.sh y un log de los contenedores de Docker para mostrar que están en funcionamiento.

![](img/deploy_logs.png)

## Gestión Agile

La Gestión Agile planteada para esta práctica es muy similar a la llevada a cabo en los laboratorios anteriores. De esta manera, las siguientes secciones detallan como se hace uso de las herramientas ofrecidas por GitLab para seguir una correcta metodología Ágil y desarrollar el proyecto satisfactoriamente.

### Milestones

Los milestones de GitLab se asemejan a los Sprints llevados a cabo dentro de la metodología Ágil, Scrum. Tras la finalización de cada uno de estos Milestones o Sprints se presenta un incremento, esto es, una pieza funcional del desarrollo del producto.

En el desarrollo presentado en este documento se detallan tres distintos Milestones, cada uno de ellos con una duración aproximada de entre cuatro y cinco días. En los siguientes apartados se describen brevemente los objetivos de estos Milestones o Sprints y se intentan aclarar las razones de la organización planteada.

#### Milestone 1

El objetivo de este primer milestone es la puesta a punto de la primera versión de la aplicación de Orders y la realización de una correcta configuración del repositorio para seguir la metodología Ágil. 
Además de la puesta a punto, se plantea también realizar la construcción del pipeline de CI (Continuous Integration) en este primer Milestone. De esta manera, es posible tener el pipeline preparado y ejecutándose para validar el software de forma correcta durante todo el desarrollo.

El pipeline creado en este primer Milestone tendrá incluidas las fases de build, test, delivery y deploy. En algunos de esos caso, como por ejemplo, test, al no existir ningún test a ejecutar en esta primera versión se construirá esa fase con intención de validar el software en cualquier caso.

El incremento de este primer Milestone será una primera versión del software, a la cual le seguirá un proceso de desarrollo validado en todo momento de forma automática mediante un pipeline de CI.

![](img/milestone_1.png)

#### Milestone 2

Este segundo Milestone tiene como objetivo añadir testing a la versión de la aplicación desplegada. Para ello, se añadirán tanto tests unitarios como tests de aceptación y/o sistema mediante un Smoke Test Suite. 

Los cambios que supongan el desarrollo de este nuevo Milestone deberán verse reflejados también en el pipeline de CI desarrollado en el Milestone anterior. En este caso, por ejemplo, los tests añadidos se deberán ejecutar en la fase test del pipeline para validar el software.

Como incremento, tras la finalización de este Milestone, se presentará una versión del software correctamente testeada.

![](img/milestone_2.png)

#### Milestone 3

El objetivo de este último Milestone es desarrollar una nueva funcionalidad o nuevas funcionalidades en la aplicación siguiendo la metodología TDD (Test Driven Development). El pipeline, como en casos anteriores, deberá reflejar el desarrollo nuevo relizado, por ejemplo, nuevos tests.

Como nueva funcionalidad se establece la obligatoriedad de implementar un nuevo método en la API con opción de ofrecerle al usuario la oportunidad de actualizar el importe monetario disponible en su cartera virtual. Además, se plantea el desarrollo de las siguientes funcionalidades opcionales y deseables: opción de eliminar un perfil de usuario y opción de eliminar la cartera virtual de un usuario.

El incremento presentado al finalizar este Milestone, será una nueva versión mejorada del software con la nueva o nuevas funcionalidades implementadas y correctamente testeadas. Además, validando de forma continua y automática la aplicación con el pipeline de CI.

![](img/milestone_3.png)

### Board

Para el desarrollo de esta práctica se ha seleccionado como en casos anteriores seguir el 'Workflow Tracking Board'. Este board define los diferentes estados en los que las tareas de desarrollo se pueden encontrar (en desarrollo, en revisión, terminado, etc.), es decir, describen todos los estados de un flujo o 'Workflow' de desarrollo y el estado en el que se encuentran las tareas individualmente.

Por defecto cualquier nuevo Board creado en GitLab contiene dos columnas o listas (Open y Closed) para las tareas que están abiertas y para desarrollar y aquellas que ya se han realizado. En el caso de este proyecto, la lista Open se puede considerar como un Product Backlog donde se reunen todas las historias de usuario a cumplir por el producto de forma ordenada en base a su prioridad. La lista, Closed, en cambio, hace referencia a las historias de usuario que ya se han desarrollado durante alguno de los Milestones. Podría asemejarse a una lista con el nombre 'Done'.

Además de estas listas creadas por defecto en GitLab, se han creado también cuatro nuevas listas o etiquetas que ayudan a complementar los estados de las tareas y definir el flujo entre la creación de la historia de usuario y su finalización.

![](img/labels.png)

Por lo tanto, estas nuevas etiquetas componen el flujo de trabajo junto con las anteriores 'Open' y 'Closed' por defecto:

1. **Desirable**: En esta columna se recopilan futuros deseos o nuevas ideas surgidas para un posible desarrollo futuro.
2. **To Do**: En esta columna se recopilan que están por desarrollar en el Sprint actual.
3. **Doing**: En esta columna se recopilan las tareas que están activas o en proceso de desarrollo.
4. **Review**: En esta columa se recopilan las tareas que se encuentran en revisión. Las tareas terminadas de la lista 'Doing' pasan a este estado para ser revisadas por una segunda o tercera persona.

El estado inicial del Board implementado se muestra en la siguiente imagen. Como se puede observar, las tareas del primer Milestone se encuentran ya en la lista 'To Do' que sería similar a un 'Sprint Backlog'. Por otro lado, todas las tareas restantes de otros Milestones se encuentran situadas en la columna 'Open', tal y como se ha explicado en líneas anteriores, simulando un 'Product Backlog'. Y finalmente, las tareas que ya en el último Milestone se han marcado como opcionales y deseables se han añadido a la lista 'Desirable' como implementaciones interesantes para un desarrollo futuro.

![](img/board_init.png)

### Issues

Las Issues como ya se mecionó en la propuesta realizada, son equivalentes a una historia de usuario de la metodología Ágil, Scrum, y por tanto, son escritas siguiendo sus convenciones. Esto es, se define un título corto y una descripción que define una tarea a realizar por un actor de la aplicación para un fin concreto.

De esta forma, el desarrollador tiene claro que y como lo tiene que hacer agilizando el trabajo. Además de la descripción, la Issue o historia de usuario es asignada a un Milestone, un desarrollador que se ocupa de su correcta realización y un Due Date, esto es, una fecha límite para la finalización de esa historia de usuario que en varias ocasiones coincide con la fecha límite de finalización del Milestone al cual pertecene la Issue.

Estas Issues son creadas al comienzo del desarrollo del producto o antes de comenzar con un nuevo Sprint, de ninguna manera es posible alterar el desarrollo de un Sprint con nuevas funcionalidades. En ese caso, se espera a que el Sprint finalice y en el Sprint Retrospective Meeting se realiza una puesta en común y se toma una decisión para impllementar esa nueva funcionalidad en el siguiente Sprint.

Como peculiaridad de esta práctica, y gracias a la nueva versión 12.8 de GitLab, es posible asignar pesos a las Issues en la versión gratuita. Por lo tanto, para el desarrollo de esta práctica se ha hecho uso de esta nueva funcionalidad con objetivo de usar ese peso como estimación del esfuerzo necesario para completar cada una de las historias de usuario. Esa estimación de esfuerzo se ha realizado, haciendo uso de las llamadas cartas de Poker utilizadas en la metodología Scrum, donde, las cartas siguen una serie numérica ascendente donde cada uno de los valores refleja un mayor esfuerzo necesario. Esta opción es efectiva también para ir analizando un Burndown Chart en cada Sprint y monitorizar si el progreso del trabajo es el adecuado en todo momento.

Finalmente, durante el desarrollo de esta práctica también se valorará el hacer uso de otra nueva funcionalidad de la versión 12.8 de GitLab, las blocking Issues. Mediante las blocking Issues, es posible indicar al equipo de trabajo que una historia de usuario se ha quedado bloqueada por algún problema o porque es dependiente de alguna otra historia de usuario. Durante el desarrollo, se irá valorando si esta funcionalidad puede encajar en un equipo de un único desarrollador. En un principio, se cree que es una funcionalidad más orientada a equipos formados por varias personas.

La siguiente imagen, muestra una Issue del proyecto y la información que contiene.

![](img/issue.png)

### Branching Strategy

La estrategia de Branching que se seguirá durante el desarrollo de este proyecto es la definida por GitHub y su 'GitHub Flow'. Esta estrategia define que todo lo que se encuentre en la rama master es desplegable, y por lo tanto, durante el desarrollo se crean nuevas ramas con un nombre descriptivo para la funcionalidad que se está desarrollando. Cuando esta funcionalidad se termina de desarrollar se abre un Pull Request a la rama master que es revisado por un tercero e integrado y desplegado directamente.

Se ha optado por seguir esta estrategia de Branching, ya que, el proyecto que se desarrolla no es de grandes dimesiones, y por lo tanto, abrir muchas ramas puede dificultar el desarrollo. Además, el equipo de desarrollo está compuesto por una sola persona, por lo que, separar en nuevas ramas las nuevas funcionalidades es conveniente en caso de querer evitar problemas en producción, pero a la vez, trabajar más centrado en la rama master y en versiones desplegables es más fácil para un único desarrollador que trabajar sobre la rama develop, por ejemplo.

![](img/github_flow.png)

Por último, citar brevemente que los mensajes de commit durante el desarrollo seguirán la estructura definida por las siguientes 6 reglas:

1. **Separar el título** de la descripción por una línea en blanco.
2. Limitar el **título a 50 carácteres**.
3. No terminar el título con un punto.
4. Usar el **imperativo** en el título.
5. Ajustar la **descripción a 72 carácteres**.
6. **La descripción debe explicar como y por qué**. 

### Reflexiones

#### Milestone 1

Las reflexiones obtenidas tras la finalización de este primer Milestone son considerables. En primer lugar, se ha conseguido presentar el incremento propuesto, el cual, consta de una primera aplicación funcional y un pipeline con las fases build, test y delivery correctamente construidas y en funcionamineto, lo que se valora positivamente.

Aún y todo, sobretodo durante la construcción del pipeline, se han ido resolviendo gran cantidad de problemas encontrados durante la fase de desarrollo. Al estar trabajando sobre docker-compose y no sobre contenedores de docker directamente, la estrategia de construcción del pipeline seguida en la anterior práctica se ha visto modificada. Los siguientes puntos resumen de forma breve esto cambios.

1. Primeramente, se ha eliminado la fase de deploy, la cual, se plantea hacer de forma manual más adelante mediante los recursos de AWS.
2. Se han creado distintos docker-compose.{proposito}.yml para diferentes propósitos. Para la fase build se mantiene el docker-compose.yml original, y mediante el, se hace build de todas las imágenes y se envían al container registry correctamente taggeadas en base al commit que se esté llevando a cabo. En la siguiente fase test, en cambio, la necesidad de hacer build de las imágenes desaperece, y lo necesario en ese caso, es realizar un pull de las imágenes desde el container registry para ponerlas en marcha y ejecutar los tests. Esto supone la necesidad de tener un nuevo docker-compose.test.yml que únicamente haga pull de las imágenes en el registry y no build.
3. Una vez pasada la fase test, en la cual, en la actualidad únicamente se ejecuta el análisis de código estático, se pasa a la fase delivery donde se taggean las imágenes para una nueva release y para el deploy. Cabe destacar también, que en la fase de test se ha descartado el uso de la herramienta de calidad de código flake8, ya que, existían conflictos entre los requisitos de las diferentes herramientas de análisis de código estático, y por tanto, se ha optado por eliminar flake8 y optar por black e isort por la facilidad y automatización que ofrecen a la hora de corregir las recomendaciones ofrecidas.

Finalmente, la siguiente imagen muestra el Burndown Chart obtenido tras la finalización de este Milestone, en este caso, se muestra una bajada mas o menos progresiva, por lo tanto, se observa un progreso constante y adecuado.

![](img/burndown_1.png)

#### Milestone 2

Tras la finalización de este segundo Milestone se ha conseguido cumplir con los objetivos y presentar el incremento propuesto que consta de la aplicación correctamente testeada mediante tests de integración, test unitarios y tests de aceptación.

Durante el desarrollo de este incremento se ha conseguido comprender de manera más profunda las diferencias que tiene cada tipo de test y las ventajas que ofrece cada uno de los tipos. Se ha entendido que los test unitarios se deben realizar por cada microservicio y lo más aislados posible, esto es, mockeando las funcionalidades externas. Por otro lado, se ha comprobado también que los tests de integración deben realizarse a nivel de cada servicio pero sin necesidad de ese aislamiento, y finalmente, se ha entendido la función que tienen los tests de aceptación que deben de comprobar el funcionamiento de la aplicación completa en un entorno lo más parecido al de producción.

Junto los últimos tests de aceptación, se ha comprendido también en que consta la fase staging del pipeline, en la cual, se comprueba el comportamiento de la aplicación mediante los tests de aceptación antes de pasar a la última fase de deploy.

Cabe destacar también, que durante el desarrollo de este incremento se han ido encontrando varios problemas que se listan a continuación:
1. La gestión de la base de datos ha necesitado ser migrada del uso de la librería SQLAlchemy común a la librería que integra Flask y SQLAlchemy, Flask-SQLAlchemy. Esto es debido a que el acceso a la base de datos en los tests no se podía hacer de forma adecuada al no estar asignada la sesión de la base de datos a la sesión de Flask.
2. La gestión de las conexiones de RabbitMQ ha necesitado también de ligeros cambios, ya que, los hilos a la escucha de mensajes en los diferentes microservicios se mantenían activos tras la finalización de los tests, y por tanto, los tests se mantenían también en ejecución. Esto se ha resuelto, enviando de mensajes de stop a esos hilos en ejecución para terminarlos.
3. Se ha incluido una nueva fase de staging en el Pipeline de CI, donde se lanza un nuevo contenedor de Docker que se encarga de la ejecución de los tests de aceptación y de interpretar el resultado. Para esta fase se ha creado también un nuevo docker-compose.staging.yml encargado de descargar las imágenes del release y hacer build del nuevo contenedor que ejecuta los Smoke Tests.

Finalmente, la siguiente imagen muestra el Burndown Chart obtenido tras la finalización de este Milestone, en este caso, la bajada y el número de Issues llevados a cabo ha sido elevado al final del Sprint, mientras que, al inicio el trabajo ha ido parado. Es algo que se debería mejorar para los siguientes Milestones o Sprints.

![](img/burndown_2.png)

#### Milestone 3

Este tercer Milestone ha sido ligeramente diferente a los anteriores, al cambiar las fechas de entrega, ha cambiando también la organización y las historias de usuario para este Milestone.

Se ha añadido una nueva historia de usuario de desarrollo de una nueva funcionalidad de eliminación de un Payment, para desarrollar junto con la anterior funcionalidad planteada de eliminación de un cliente. 

Además de la nueva Issue anterior, se ha añadido también una Issue correspondiente al stage de deploy, ya que, aunque este se realiza de forma manual si que se ha terminado añadiendo algunos scripts que ayudan a facilitar el trabajo.

Todos estos cambios, se han llevado a cabo antes de comenzar con el Milestone, tal y como recomiendo la metodología Ágil, Scrum. Por lo tanto, se puede concluir que el incremento de este Milestone se ha cumplido también forma satisfactoria, ya que, se ha presentado la nueva funcionalidad implementada de forma más robusta y ha sido posible también desarrollar algunas de las historias de usuario que han ido apareciendo durante el desarrollo y se han ido marcando como "Desirable": mockear las conexiones de RabbitMQ en los tests unitarios y realizar un nuevo Smoke Test con las nuevas funcionalidades implementadas.

En caso de estas historias de usuario "Desirable" llevada a cabo, sus Merge Requests se han marcado también con el label "Desirable" para manejar así de forma más adecuada la navegación entre ellos y su compresión. 

Finalmente, cabe destacar que en este último Sprint o Milestone, el Burndown Chart tiene una apariencia parecida al anterior, las Issues se han acumulado al final, aunque luego se hayan podido implementar Issues "Desirable". Esto, por tanto, es algo a tener en cuenta para futuros proyectos.

![](img/burndown_3.png)