# **P1 -** **Validated** **Container** **Delivery**
[![pipeline status](https://gitlab.com/mikel_amuchastegui/p1-validated-container-delivery/badges/master/pipeline.svg)](https://gitlab.com/mikel_amuchastegui/p1-validated-container-delivery/-/commits/master)[![coverage report](https://gitlab.com/mikel_amuchastegui/p1-validated-container-delivery/badges/master/coverage.svg)](https://gitlab.com/mikel_amuchastegui/p1-validated-container-delivery/-/commits/master)

Este repositorio contiene la **Práctica 1º** denominada **Validated Container Delivery** de la asignatura **Integración y Despliegue Continuo** del master [Análisis de datos, Ciberseguridad y Computación en la nube](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube)

La práctica tiene por objetivo ser capaz de **diseñar e implementar un pipeline CI** para el sistema de pedidos (“Orders”). 

## Changelog

Con el desarrollo de esta práctica se ha liberado una nueva release del proyecto Orders. A continuación, los principales cambios:

* Nuevo test suite de **pruebas unitarias** con el se proporciona una cobertura del 20%.
* Nuevo **Smoke Test Suite** para el proyecto.
* Nueva **funcionalidad** que ofrece la posibilidad de actualizar el dinero disponible en la cartera de un usuario.
* Se ha diseñado, implementado y desplegado el **pipeline CI**.
* Se ha desarrollado un script de comandos SSH para el **despliegue manual** en Stage de la release.
* Se ha gestionado el desarrollo en base a la **metodología ágil** descrita en `doc.md`

## Documentation

Los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica quedan recogidos en la documentación complementaria [doc.md](doc.md).

## Authors

Mikel Amuchastegui Zubizarreta – @mikelamutxas – [mikel.amuchastegui@alumni.mondragon.edu](mailto:mikel.amuchastegui@alumni.mondragon.edu)

Quiero agradecer el apoyo de Joseba Andoni y Urtzi en sus revisiones.

##  License

This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/),  see the `LICENSE.md` file for details.
