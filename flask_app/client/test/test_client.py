import json

import pytest
import requests


def test_add_client_unssuported_type(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/client",
        data=json.dumps({"username": "mikel"}),
        content_type="application/xml",
    )
    assert resp.status_code == 415


def test_add_client(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/client",
        data=json.dumps({"username": "mikel"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["username"] == "mikel"


def test_get_client(test_app, add_client, test_database):
    new_client = add_client("joseba")
    client = test_app.test_client()
    resp = client.get(
        "/client/" + str(new_client.username), content_type="application/json"
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["username"] == "joseba"


def test_get_client_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/client/antton", content_type="application/json")
    assert resp.status_code == 404


def test_get_all_clients(test_app, test_database, add_client):
    client = test_app.test_client()
    add_client("antton")
    add_client("xabi")
    add_client("mikel")
    resp = client.get("/client", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 3


def test_delete_client(test_app, test_database, add_client):
    client = test_app.test_client()
    added_client = add_client("client_to_delete")
    resp = client.delete(
        "/client/" + str(added_client.username), content_type="application/json"
    )
    assert resp.status_code == 200


def test_delete_client_unssuported_type(test_app, test_database, add_client):
    client = test_app.test_client()
    added_client = add_client("client_to_delete")
    resp = client.delete(
        "/client/" + str(added_client.username), content_type="application/xml"
    )

    assert resp.status_code == 415


def test_delete_client_incorrect_id(test_app, test_database, add_client):
    client = test_app.test_client()
    resp = client.delete("/client/idontknow", content_type="application/json")
    assert resp.status_code == 404
