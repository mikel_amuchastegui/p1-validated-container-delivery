import datetime
import json

import pytest

from application.businessLogic import BusinessLogic
from application.EventPublisher import EventPublisher
from application.models import Client


def test_add_client(test_app, monkeypatch):
    def mock_create_client(username):
        return {"username": username}

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "create_client", mock_create_client)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)

    client = test_app.test_client()
    resp = client.post(
        "/client",
        data=json.dumps({"username": "mikel"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["username"] == "mikel"


def test_get_client(test_app, monkeypatch):
    def mock_get_client(client_id):
        return Client(username=client_id)

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_client", mock_get_client)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)

    client = test_app.test_client()
    resp = client.get("/client/joseba", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["username"] == "joseba"


def test_get_all_clients(test_app, monkeypatch):
    def mock_get_all_client():
        return [
            Client(username="antton"),
            Client(username="xabi"),
            Client(username="mikel"),
        ]

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_all_client", mock_get_all_client)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)

    client = test_app.test_client()
    resp = client.get("/client", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 3
