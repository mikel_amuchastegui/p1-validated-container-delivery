import pytest

from application import create_app, db, models
from application.models import Client


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    app.config.from_object("application.config.TestingConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.create_all(db.engine)
    yield db
    models.Base.metadata.drop_all(db.engine)


@pytest.fixture(scope="function")
def add_client():
    def _add_client(username):
        client = Client(username=username)
        db.session.add(client)
        db.session.commit()
        return client

    return _add_client
