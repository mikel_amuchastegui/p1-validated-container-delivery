import traceback

from flask import abort
from flask import current_app as app
from flask import jsonify, request

from .api_client import delete_payment
from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher
from .models import Client

from werkzeug.exceptions import (  # isort:skip
    BadRequest,
    InternalServerError,
    NotFound,
    UnsupportedMediaType,
)

eventPublisher = EventPublisher("client")


@app.route("/client", methods=["POST"])
def create_client():
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json
    username = content["username"]
    new_client_dict = BusinessLogic.create_client(username)
    if new_client_dict is None:
        eventPublisher.send_data(data="POST_ERROR creating new client", routing_key="")
        abort(BadRequest.code)
    else:
        eventPublisher.send_data(
            data="POST new client created: " + str(new_client_dict), routing_key=""
        )
        response = jsonify(new_client_dict)

    return response


@app.route("/clients", methods=["GET"])
@app.route("/client", methods=["GET"])
def get_all_client():
    clients = BusinessLogic.get_all_client()
    response = jsonify(Client.list_as_dict(clients))
    return response


@app.route("/client/<string:client_id>", methods=["GET"])
def get_client(client_id):
    client = BusinessLogic.get_client(client_id)
    if client is None:
        abort(NotFound.code)
    print("GET Client {}: {}".format(client_id, client))
    response = jsonify(client.as_dict())
    return response


@app.route("/client/<string:client_id>", methods=["DELETE"])
def delete_client(client_id):
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    client = BusinessLogic.delete_client(client_id)
    if client is None:
        abort(NotFound.code)
    delete_payment(client_id)
    response = jsonify({client_id: "deleted succesfully"})
    return response


@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    return "OK"


@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
