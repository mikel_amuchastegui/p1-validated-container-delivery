from . import db
from .models import Client


class BusinessLogic:
    @staticmethod
    def create_client(client_id):
        new_client = None
        try:
            new_client = Client(username=client_id)
            db.session.add(new_client)
            db.session.commit()
            new_client_dict = new_client.as_dict()
            db.session.close()
            return new_client_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def get_all_client():
        clients = db.session.query(Client).all()
        db.session.close()
        return clients

    @staticmethod
    def get_client(client_id):
        client = db.session.query(Client).get(client_id)
        if not client:
            db.session.close()
            return None
        db.session.close()
        return client

    @staticmethod
    def delete_client(client_id):
        client = db.session.query(Client).get(client_id)
        if not client:
            db.session.close()
            db.session.rollback()
            return None
        db.session.delete(client)
        db.session.commit()
        db.session.close()
        return client
