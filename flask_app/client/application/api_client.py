import json
from os import environ

import requests

headers = {"Content-type": "application/json"}


def delete_payment(client_id):
    payment_response = requests.delete(
        url=environ.get("HAPROXY_IP") + "/payment/" + str(client_id), headers=headers,
    )
    return payment_response
