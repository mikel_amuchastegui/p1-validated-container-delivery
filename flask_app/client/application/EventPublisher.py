#!/usr/bin/env python
import time
from os import environ

import pika
from pika import exceptions


class EventPublisher:
    def __init__(self, exchange):
        self.exchange = exchange

    def open_connection(self, exchange):
        while True:
            try:
                self.connection = pika.BlockingConnection(
                    pika.ConnectionParameters(host=environ.get("RABBITMQ_IP"))
                )
                self.channel = self.connection.channel()
                self.channel.exchange_declare(
                    exchange=exchange, exchange_type="fanout", durable=True
                )
                self.exchange = exchange
            except exceptions.AMQPConnectionError:
                print("Error connecting to RMQ")
                time.sleep(5)
                continue
            print("Connection stablished")
            break

    def send_data(self, data, routing_key):
        data = str(data)
        self.open_connection(self.exchange)
        self.channel.basic_publish(
            exchange=self.exchange, routing_key=routing_key, body=data
        )

        print(" [x] Sent " + data)
        self.close()

    def close(self):
        self.connection.close()
