import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import models
        from . import routes

        app_settings = os.getenv("APP_SETTINGS")
        app.config.from_object(app_settings)
        db.init_app(app)

        models.Base.metadata.create_all(db.engine)
        return app
