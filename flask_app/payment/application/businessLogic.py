from . import db
from .models import Payment


class BusinessLogic:
    @staticmethod
    def update_payment(user_id, balance):
        try:
            payment = db.session.query(Payment).get(user_id)
            if not payment:
                print("not payment")
                return None
            if payment.balance - balance < 0:
                print("not enough balance")
                return None
            print("updating balance")
            payment.balance -= balance
            db.session.commit()
            new_payment_dict = payment.as_dict()
            db.session.close()
            return new_payment_dict

        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def create_payment(user_id, balance):
        new_payment = None
        try:
            new_payment = Payment(user_id=user_id, balance=balance)
            db.session.add(new_payment)
            db.session.commit()
            new_payment_dict = new_payment.as_dict()
            db.session.close()
            return new_payment_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def get_payment(user_id):
        payment = db.session.query(Payment).get(user_id)
        if not payment:
            db.session.close()
            return None
        db.session.close()
        return payment

    @staticmethod
    def delete_payment(user_id):
        payment = db.session.query(Payment).get(user_id)
        if not payment:
            db.session.close()
            db.session.rollback()
            return None
        db.session.delete(payment)
        db.session.commit()
        db.session.close()
        return payment
