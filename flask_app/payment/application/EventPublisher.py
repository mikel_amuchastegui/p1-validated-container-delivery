#!/usr/bin/env python
from os import environ

import pika


class EventPublisher:
    def __init__(self, exchange):
        print("Payment")
        rabbitmq_ip = environ.get("RABBITMQ_IP")
        pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
        self.connection = pika.BlockingConnection(pika_conn_param)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange=exchange, exchange_type="fanout", durable=True
        )
        self.exchange = exchange

    def send_data(self, data, routing_key):
        data = str(data)
        print("exchange: " + self.exchange)
        self.channel.basic_publish(
            exchange=self.exchange,
            routing_key=routing_key,
            body=data,
            properties=pika.BasicProperties(delivery_mode=2,),
        )

        print(" [x] Sent " + data)

    def close(self):
        self.connection.close()
