import traceback

from flask import abort
from flask import current_app as app
from flask import jsonify, request

from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher

from werkzeug.exceptions import (  # isort:skip
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
    HTTPException,
)

eventPublisher = EventPublisher("payment")


@app.route("/payment", methods=["POST"])
def create_payment():
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json
    new_payment_dict = BusinessLogic.create_payment(
        content["user_id"], content["balance"]
    )
    if new_payment_dict is None:
        abort(BadRequest.code)
    else:
        eventPublisher.send_data(
            data="POST new Payment created: " + str(new_payment_dict), routing_key=""
        )
        response = jsonify(new_payment_dict)
    return response


@app.route("/payment/<string:user_id>", methods=["GET"])
def view_payment(user_id):
    payment = BusinessLogic.get_payment(user_id)
    if not payment:
        abort(NotFound.code)
    print("GET Client {}: {}".format(user_id, payment))
    response = jsonify(payment.as_dict())
    return response


@app.route("/payment/<string:user_id>", methods=["PUT"])
def update_balance(user_id):
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json
    new_payment_dict = BusinessLogic.update_payment(user_id, content["balance"])
    if new_payment_dict is None:
        eventPublisher.send_data(
            data="PUT_ERROR changing Payment status", routing_key=""
        )
        abort(NotFound.code)
    else:
        eventPublisher.send_data(
            data="PUT Payment status changed to: " + str(new_payment_dict),
            routing_key="",
        )
    response = jsonify(new_payment_dict)
    return response


@app.route("/payment/<string:user_id>", methods=["DELETE"])
def delete_payment(user_id):
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    payment = BusinessLogic.delete_payment(user_id)
    if payment is None:
        abort(NotFound.code)
    response = jsonify({user_id: "deleted succesfully"})
    return response


@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    return "OK"


@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code


class PaymentRequired(HTTPException):
    code = 402
    description = "<p>Payment required.</p>"
