#!/usr/bin/env python

import json
import time
from os import environ
from threading import Thread

import pika
from dotenv import load_dotenv
from pika import exceptions

from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher

load_dotenv()


class EventHandler(Thread):
    def __init__(self, exchange, routing_key, type, app):
        Thread.__init__(self)
        while True:
            try:
                rabbitmq_ip = environ.get("RABBITMQ_IP")
                pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
                self.connection = pika.BlockingConnection(pika_conn_param)

            except exceptions.AMQPConnectionError:
                print("Error connecting to RMQ")
                time.sleep(5)
                continue
            print("Connection established")
            break

        self.channel = self.connection.channel()
        self.type = type
        self.channel.exchange_declare(
            exchange=exchange, exchange_type="fanout", durable=True
        )
        result = self.channel.queue_declare(queue="", durable=True)
        self.channel.queue_bind(
            exchange=exchange, queue=result.method.queue, routing_key=routing_key
        )
        self.channel.basic_consume(
            queue=result.method.queue, auto_ack=False, on_message_callback=self.callback
        )
        self.app = app
        self.start()

    def run(self):
        print(" [*] Waiting for messages. To exit press CTRL+C")
        with self.app.app_context():
            self.channel.start_consuming()

    def callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body)
        jsonMsg = json.loads(body)
        if "stop" in jsonMsg and jsonMsg["stop"] == True:
            self.channel.stop_consuming()
        else:
            status = BusinessLogic.update_payment(
                jsonMsg["client_id"], jsonMsg["balance"]
            )
            if status:
                event = "payment_done"
            else:
                event = "payment_error"
            exchange_response = jsonMsg["exchange_response"]
            eventPublisher = EventPublisher(exchange_response)
            print("Sending event: " + event)
            eventPublisher.send_data(json.dumps({"event": event}), "")
            ch.basic_ack(delivery_tag=method.delivery_tag)
