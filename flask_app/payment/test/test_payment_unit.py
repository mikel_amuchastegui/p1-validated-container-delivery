import datetime
import json

import pytest

from application.businessLogic import BusinessLogic
from application.EventPublisher import EventPublisher
from application.models import Payment


def test_add_payment(test_app, monkeypatch):
    def mock_create_payment(user_id, balance):
        return {"user_id": user_id, "balance": balance}

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "create_payment", mock_create_payment)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.post(
        "/payment",
        data=json.dumps({"user_id": "mikel", "balance": 300}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["user_id"] == "mikel"
    assert data["balance"] == 300


def test_view_payment(test_app, monkeypatch):
    def mock_get_payment(user_id):
        return Payment(user_id=user_id, balance=1000)

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_payment", mock_get_payment)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.get("/payment/joseba", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["user_id"] == "joseba"
    assert data["balance"] == 1000


def test_update_balance(test_app, monkeypatch):
    def mock_update_payment(user_id, balance):
        return {"user_id": user_id, "balance": balance}

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "update_payment", mock_update_payment)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.put(
        "/payment/urtzi",
        data=json.dumps({"balance": 50}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["user_id"] == "urtzi"
    assert data["balance"] == 50
