import json

import pytest


def test_add_payment_unssuported_type(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/payment",
        data=json.dumps({"user_id": "mikel", "balance": 300}),
        content_type="application/xml",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 415


def test_add_payment(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/payment",
        data=json.dumps({"user_id": "mikel", "balance": 300}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["user_id"] == "mikel"
    assert data["balance"] == 300


def test_view_payment(test_app, add_payment, test_database):
    payment = add_payment("joseba", 1000)
    client = test_app.test_client()
    resp = client.get(
        "/payment/" + str(payment.user_id), content_type="application/json"
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["user_id"] == "joseba"
    assert data["balance"] == 1000


def test_view_payment_incorrect_user_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/payment/unai", content_type="application/json")
    assert resp.status_code == 404


def test_update_payment_unssuported_type(test_app, add_payment, test_database):
    payment = add_payment("urtzi", 100)
    client = test_app.test_client()
    resp = client.put(
        "/payment/" + str(payment.user_id),
        data=json.dumps({"balance": 50}),
        content_type="application/xml",
    )
    assert resp.status_code == 415


def test_update_balance(test_app, add_payment, test_database):
    payment = add_payment("urtzi", 100)
    client = test_app.test_client()
    resp = client.put(
        "/payment/" + str(payment.user_id),
        data=json.dumps({"balance": 50}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["user_id"] == "urtzi"
    assert data["balance"] == 50


def test_update_balance_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.put(
        "/delivery/unai",
        data=json.dumps({"balance": 200}),
        content_type="application/json",
    )
    assert resp.status_code == 404


def test_delete_payment(test_app, test_database, add_payment):
    payment = add_payment("payment_to_delete", 500)
    client = test_app.test_client()
    resp = client.delete(
        "/payment/" + str(payment.user_id), content_type="application/json"
    )
    assert resp.status_code == 200


def test_delete_payment_unssuported_type(test_app, test_database, add_payment):
    payment = add_payment("payment_to_delete", 500)
    client = test_app.test_client()
    resp = client.delete(
        "/payment/" + str(payment.user_id), content_type="application/xml"
    )
    assert resp.status_code == 415


def test_delete_payment_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.delete("/payment/idontknow", content_type="application/json")
    assert resp.status_code == 404
