import json

import pytest

from application import create_app, db, models
from application.EventPublisher import EventPublisher
from application.models import Payment


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    app.config.from_object("application.config.TestingConfig")
    with app.app_context():
        yield app
        evPublish = EventPublisher("payment_saga")
        evPublish.send_data(json.dumps({"stop": True}), "")


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.create_all(db.engine)
    yield db
    models.Base.metadata.drop_all(db.engine)


@pytest.fixture(scope="function")
def add_payment():
    def _add_payment(user_id, balance):
        payment = Payment(user_id=user_id, balance=balance)
        db.session.add(payment)
        db.session.commit()
        return payment

    return _add_payment
