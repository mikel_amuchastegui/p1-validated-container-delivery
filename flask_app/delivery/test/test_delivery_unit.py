import json

import pytest

from application.businessLogic import BusinessLogic
from application.EventPublisher import EventPublisher
from application.models import Delivery


def test_add_delivery(test_app, monkeypatch):
    def mock_create_delivery(order_id, user_id, address):
        return {"order_id": order_id, "user_id": str(user_id), "address": address}

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "create_delivery", mock_create_delivery)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.post(
        "/delivery",
        data=json.dumps({"order_id": 1, "user_id": 1, "address": "48"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 1
    assert data["user_id"] == "1"
    assert data["address"] == "48"


def test_view_delivery(test_app, add_delivery, monkeypatch):
    def mock_get_delivery(delivery_id):
        return Delivery(id=1, order_id=1, user_id=1, address="48")

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_delivery", mock_get_delivery)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.get("/delivery/1", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 1
    assert data["user_id"] == 1
    assert data["address"] == "48"


def test_update_delivery(test_app, monkeypatch):
    def mock_update_delivery(delivery_id, status):
        return {"order_id": delivery_id, "status": status}

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "update_delivery", mock_update_delivery)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.put(
        "/delivery/1",
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["status"] == "Finished"


def test_view_order_delivery(test_app, monkeypatch):
    def mock_get_order_delivery(order_id):
        return Delivery(order_id=20, address="20500")

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_order_delivery", mock_get_order_delivery)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)

    client = test_app.test_client()
    resp = client.get(
        "/delivery/order/1",
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 20
    assert data["address"] == "20500"
