import json

import pytest


def test_add_delivery_unssuported_type(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/delivery",
        data=json.dumps({"order_id": 1, "user_id": 1, "address": "48"}),
        content_type="application/xml",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 415


def test_add_delivery(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/delivery",
        data=json.dumps({"order_id": 1, "user_id": 1, "address": "48"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 1
    assert data["user_id"] == "1"
    assert data["address"] == "48"


def test_view_delivery(test_app, add_delivery, test_database):
    delivery = add_delivery(2, 2, "20500")
    client = test_app.test_client()
    resp = client.get("/delivery/" + str(delivery.id), content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 2
    assert data["user_id"] == "2"
    assert data["address"] == "20500"


def test_view_delivery_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/delivery/99999", content_type="application/json")
    assert resp.status_code == 404


def test_update_delivery_unssuported_type(test_app, add_delivery, test_database):
    delivery = add_delivery(30, 30, "20500")
    client = test_app.test_client()
    resp = client.put(
        "/delivery/" + str(delivery.id),
        data=json.dumps({"status": "Finished"}),
        content_type="application/xml",
    )
    assert resp.status_code == 415


def test_update_delivery(test_app, add_delivery, test_database):
    delivery = add_delivery(30, 30, "20500")
    client = test_app.test_client()
    resp = client.put(
        "/delivery/" + str(delivery.id),
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["status"] == "Finished"


def test_update_delivery_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.put(
        "/delivery/999999",
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    assert resp.status_code == 404


def test_view_order_delivery(test_app, add_delivery, test_database):
    delivery = add_delivery(20, 20, "20500")
    client = test_app.test_client()
    resp = client.get(
        "/delivery/order/" + str(delivery.order_id),
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 20
    assert data["address"] == "20500"


def test_view_order_delivery_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/delivery/order/99999", content_type="application/json")
    assert resp.status_code == 404
