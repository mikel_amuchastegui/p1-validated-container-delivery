import json

import pytest

from application import create_app, db, models
from application.EventPublisher import EventPublisher
from application.models import Delivery


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    app.config.from_object("application.config.TestingConfig")
    with app.app_context():
        yield app
        evPublish = EventPublisher("delivery_saga")
        evPublish.send_data(json.dumps({"stop": True}), "")


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.create_all(db.engine)
    yield db
    models.Base.metadata.drop_all(db.engine)


@pytest.fixture(scope="function")
def add_delivery():
    def _add_delivery(order_id, user_id, address):
        delivery = Delivery(order_id=order_id, user_id=user_id, address=address)
        db.session.add(delivery)
        db.session.commit()
        return delivery

    return _add_delivery
