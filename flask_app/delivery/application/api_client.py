import json
from os import environ

import requests


def update_order(order_id, status):
    order_data = {"status": status}

    headers = {"Content-type": "application/json"}
    print("update order status: " + str(status))
    order_response = requests.put(
        url=environ.get("HAPROXY_IP") + "/order/" + str(order_id),
        data=json.dumps(order_data),
        headers=headers,
    )
    return order_response
