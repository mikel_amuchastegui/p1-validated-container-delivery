import traceback

from flask import abort
from flask import current_app as app
from flask import jsonify, request

from .api_client import update_order
from .businessLogic import BusinessLogic as bl
from .EventPublisher import EventPublisher
from .models import Delivery

from werkzeug.exceptions import (  # isort:skip
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
)


eventPublisher = EventPublisher("delivery")

ORDER_API = "http://localhost:13004"


@app.route("/delivery", methods=["POST"])
def create_delivery():
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json

    new_delivery_dict = bl.create_delivery(
        order_id=content["order_id"],
        user_id=content["user_id"],
        address=content["address"],
    )
    if new_delivery_dict is None:
        eventPublisher.send_data(
            data="POST_ERROR creating new delivery", routing_key=""
        )
        abort(BadRequest.code)
    else:
        eventPublisher.send_data(
            data="POST new delivery created: " + str(new_delivery_dict), routing_key=""
        )
        response = jsonify(new_delivery_dict)
    return response


@app.route("/delivery/<int:delivery_id>", methods=["GET"])
def view_delivery(delivery_id):
    delivery = bl.get_delivery(delivery_id)
    if delivery is None:
        abort(NotFound.code)
    print("GET Client {}: {}".format(delivery_id, delivery))
    response = jsonify(delivery.as_dict())
    return response


@app.route("/delivery/<int:delivery_id>", methods=["PUT"])
def update_delivery(delivery_id):
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json
    status = content["status"]
    delivery_dict = bl.update_delivery(delivery_id, status)
    if delivery_dict is None:
        abort(NotFound.code)
    if status == Delivery.STATUS_FINISHED:
        update_order(delivery_dict["order_id"], "Delivered")

    eventPublisher.send_data(
        data="PUT Delivery status changed to " + str(delivery_dict), routing_key=""
    )
    response = jsonify(delivery_dict)
    return response


@app.route("/delivery/order/<int:order_id>", methods=["GET"])
def view_order_delivery(order_id):
    print("order_id: " + str(order_id))
    delivery = bl.get_order_delivery(order_id)
    if delivery is None:
        abort(NotFound.code)
    print("GET Client {}: {}".format(order_id, delivery))
    response = jsonify(delivery.as_dict())
    print(response)
    return response


@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    return "OK"


@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
