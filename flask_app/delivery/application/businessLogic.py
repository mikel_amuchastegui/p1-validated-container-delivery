from . import db
from .models import Delivery


class BusinessLogic:

    CONNECTION = False

    @staticmethod
    def create_delivery(order_id, user_id, address):
        try:
            new_delivery = Delivery(
                user_id=user_id, order_id=int(order_id), address=address
            )
            db.session.add(new_delivery)
            db.session.commit()
            new_delivery_dict = new_delivery.as_dict()
            db.session.close()
            return new_delivery_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def get_delivery(delivery_id):
        delivery = db.session.query(Delivery).get(delivery_id)
        if not delivery:
            db.session.rollback()
            db.session.close()
            return None
        db.session.close()
        return delivery

    @staticmethod
    def get_order_delivery(order_id):
        delivery = (
            db.session.query(Delivery)
            .filter(Delivery.order_id == int(order_id))
            .one_or_none()
        )
        print(delivery)
        if not delivery:
            db.session.rollback()
            db.session.close()
            return None
        db.session.close()
        return delivery

    @staticmethod
    def update_delivery(delivery_id, status):
        delivery = db.session.query(Delivery).get(delivery_id)
        if not delivery:
            db.session.rollback()
            db.session.close()
            return None
        delivery.status = status
        db.session.commit()
        delivery_dict = delivery.as_dict()
        db.session.close()
        return delivery_dict

    @staticmethod
    def set_connection(status):
        BusinessLogic.CONNECTION = status
