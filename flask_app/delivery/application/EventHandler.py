#!/usr/bin/env python

import json
import time
from os import environ
from threading import Thread

import pika
from pika import exceptions

from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher
from .models import Delivery


class EventHandler(Thread):

    running = True

    def __init__(self, exchange, routing_key, type, app):
        Thread.__init__(self)
        while True:
            try:
                rabbitmq_ip = environ.get("RABBITMQ_IP")
                pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
                self.connection = pika.BlockingConnection(pika_conn_param)
            except exceptions.AMQPConnectionError:
                print("Error connecting to RMQ")
                time.sleep(5)
                continue
            print("Connection stablished")
            break

        self.type = type
        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange=exchange, exchange_type="fanout", durable=True
        )
        result = self.channel.queue_declare(queue="", durable=True)
        self.channel.queue_bind(
            exchange=exchange, queue=result.method.queue, routing_key=routing_key
        )
        self.channel.basic_consume(
            queue=result.method.queue, auto_ack=False, on_message_callback=self.callback
        )
        self.app = app
        self.start()

    def run(self):
        with self.app.app_context():
            self.channel.start_consuming()
        print("stop consuming")

    def callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body)
        jsonMsg = json.loads(body)
        if "stop" in jsonMsg and jsonMsg["stop"] == True:
            self.channel.stop_consuming()
            print("stop consuming")
        else:
            new_delivery_dict = BusinessLogic.create_delivery(
                jsonMsg["order_id"], jsonMsg["client_id"], jsonMsg["address"]
            )
            if (
                new_delivery_dict is not None
                and new_delivery_dict["address"] in Delivery.ATTEND_CODES
            ):
                event = "delivery_done"
            else:
                event = "delivery_error"
            exchange_response = jsonMsg["exchange_response"]
            eventPublisher = EventPublisher(exchange_response)
            eventPublisher.send_data(json.dumps({"event": event}), "")
            ch.basic_ack(delivery_tag=method.delivery_tag)

    def close(self):
        print("stopping")
        self.running = False
        # self.channel.stop_consuming()
