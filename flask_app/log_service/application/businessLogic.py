from . import db
from .models import Log


class BusinessLogic:
    @staticmethod
    def get_all_logs():
        logs = db.session.query(Log).all()
        db.session.close()
        return logs
