import os

from flask import Flask, current_app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import models

        app_settings = os.getenv("APP_SETTINGS")
        print(app_settings)

        app.config.from_object(app_settings)

        db.init_app(app)

        models.Base.metadata.create_all(db.engine)
        from .EventHandler import EventHandler

        EventHandler(
            exchange="payment",
            routing_key="",
            type="api",
            app=current_app._get_current_object(),
        )
        EventHandler(
            exchange="client",
            routing_key="",
            type="api",
            app=current_app._get_current_object(),
        )
        EventHandler(
            exchange="order",
            routing_key="",
            type="api",
            app=current_app._get_current_object(),
        )
        EventHandler(
            exchange="delivery",
            routing_key="",
            type="api",
            app=current_app._get_current_object(),
        )
        EventHandler(
            exchange="machine",
            routing_key="",
            type="api",
            app=current_app._get_current_object(),
        )
        from . import routes

        return app
