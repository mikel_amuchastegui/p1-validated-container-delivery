#!/usr/bin/env python
import time
from os import environ
from threading import Thread

import pika
from pika import exceptions

from . import db
from .models import Log


class EventHandler(Thread):
    def __init__(self, exchange, routing_key, type, app):
        print("LOG")
        Thread.__init__(self)
        while True:
            try:
                self.type = type
                self.exchange = exchange
                rabbitmq_ip = environ.get("RABBITMQ_IP")
                pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
                self.connection = pika.BlockingConnection(pika_conn_param)
            except exceptions.AMQPConnectionError:
                print("Error connecting to RMQ")
                time.sleep(5)
                continue
            print("Connection stablished")
            break
        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange=exchange, exchange_type="fanout", durable=True
        )
        result = self.channel.queue_declare(queue="", durable=True)
        self.channel.queue_bind(
            exchange=exchange, queue=result.method.queue, routing_key=routing_key
        )

        self.channel.basic_consume(
            queue=result.method.queue, auto_ack=False, on_message_callback=self.callback
        )
        self.app = app
        self.start()

    def run(self):
        print(" [*] Waiting for messages. To exit press CTRL+C")
        with self.app.app_context():
            self.channel.start_consuming()

    def callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body)
        if body.decode("UTF-8") == "stop":
            self.channel.stop_consuming()
        else:
            log = Log(log_from=self.exchange, data=body.decode("UTF-8"), type=self.type)
            db.session.add(log)
            db.session.commit()
            ch.basic_ack(delivery_tag=method.delivery_tag)
