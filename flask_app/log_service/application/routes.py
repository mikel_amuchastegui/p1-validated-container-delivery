import traceback

from flask import current_app as app
from flask import jsonify

from .businessLogic import BusinessLogic
from .models import Log

from werkzeug.exceptions import (  # isort:skip
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
)


@app.route("/log", methods=["GET"])
def get_logs():
    print("GET LOGS")
    print(app.config)
    logs = BusinessLogic.get_all_logs()
    response = jsonify(Log.list_as_dict(logs))
    return response


@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    return "OK"


@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e._traceback_)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
