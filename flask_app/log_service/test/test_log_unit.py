import json

import pytest

from application.businessLogic import BusinessLogic
from application.EventPublisher import EventPublisher
from application.models import Log


def test_get_logs(test_app, monkeypatch):
    def mock_get_all_logs():
        return [
            Log(log_from="test", data="dummy test message1", type="test"),
            Log(log_from="test", data="dummy test message2", type="test"),
            Log(log_from="test", data="dummy test message3", type="test"),
        ]

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_all_logs", mock_get_all_logs)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.get("/log", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data[0]["data"] == "dummy test message1"
    assert data[1]["data"] == "dummy test message2"
    assert data[2]["data"] == "dummy test message3"
