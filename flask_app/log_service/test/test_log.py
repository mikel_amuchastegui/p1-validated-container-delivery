import json

import pytest


def test_get_logs(test_app, test_database, add_log):
    add_log("test", "dummy test message1", "test")
    add_log("test", "dummy test message2", "test")
    add_log("test", "dummy test message3", "test")
    client = test_app.test_client()
    resp = client.get("/log", content_type="application/json")
    data = json.loads(resp.data.decode())
    print(data)
    assert resp.status_code == 200
    assert data[0]["data"] == "dummy test message1"
    assert data[1]["data"] == "dummy test message2"
    assert data[2]["data"] == "dummy test message3"
