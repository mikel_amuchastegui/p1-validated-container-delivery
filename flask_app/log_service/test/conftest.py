import json

import pytest

from application import create_app, db, models
from application.EventPublisher import EventPublisher
from application.models import Log


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    print(app.config.from_object("application.config.TestingConfig"))
    with app.app_context():
        yield app
        evPublish = EventPublisher("payment")
        evPublish.send_data("stop", "")
        evPublish = EventPublisher("client")
        evPublish.send_data("stop", "")
        evPublish = EventPublisher("order")
        evPublish.send_data("stop", "")
        evPublish = EventPublisher("delivery")
        evPublish.send_data("stop", "")
        evPublish = EventPublisher("machine")
        evPublish.send_data("stop", "")


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.create_all(db.engine)
    yield db
    models.Base.metadata.drop_all(db.engine)


@pytest.fixture(scope="function")
def add_log():
    def _add_log(log_from, data, type):
        log = Log(log_from=log_from, data=data, type=type)
        db.session.add(log)
        db.session.commit()
        return log

    return _add_log
