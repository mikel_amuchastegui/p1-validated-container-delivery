import json
from os import environ

import requests

from .EventPublisher import EventPublisher

eventPublisher = EventPublisher("machine")
headers = {"Content-type": "application/json"}


def update_order_status(piece_id, status):
    piece_data = {"status": status}
    print("piece " + str(piece_id) + " finished")
    print("url: " + environ.get("HAPROXY_IP") + "/piece/" + str(piece_id))
    order_response = requests.put(
        url=environ.get("HAPROXY_IP") + "/piece/" + str(piece_id),
        data=json.dumps(piece_data),
        headers=headers,
    )
    eventPublisher.send_data(
        data="PUT piece with ID: " + str(piece_id) + str(status), routing_key=""
    )

    return order_response
