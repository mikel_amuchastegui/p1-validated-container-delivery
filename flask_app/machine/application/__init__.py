from flask import Flask

from .machine import Machine

my_machine = Machine()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import routes

        return app
