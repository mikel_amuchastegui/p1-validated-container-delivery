from collections import deque
from random import randint
from threading import Event, Lock, Thread
from time import sleep

from .api_client import update_order_status


class Machine(Thread):
    STATUS_WAITING = "Waiting"
    STATUS_CHANGING_PIECE = "Changing Piece"
    STATUS_WORKING = "Working"
    __stauslock__ = Lock()
    thread_session = None
    running = True

    def __init__(self):
        Thread.__init__(self)
        self.queue = deque([])
        self.working_piece = None
        self.status = Machine.STATUS_WAITING
        self.instance = self
        self.queue_not_empty_event = Event()
        self.running = True
        self.start()

    def run(self):
        while self.running:
            self.queue_not_empty_event.wait()
            print("Thread notified that queue is not empty.")

            while self.queue.__len__() > 0:
                self.instance.create_piece()

            self.queue_not_empty_event.clear()
            print("Lock thread because query is empty.")

            self.instance.status = Machine.STATUS_WAITING

    def stop(self):
        self.running = False

    def create_piece(self):
        # Get piece from queue
        piece = self.queue.popleft()

        # Machine and piece status updated during manufacturing
        self.working_piece = piece

        # Machine and piece status updated before manufacturing
        self.working_piece_to_manufacturing()

        # Simulate piece is being manufactured
        sleep(randint(5, 6))

        # Machine and piece status updated after manufacturing
        self.working_piece_to_finished()

        self.working_piece = None

    def working_piece_to_manufacturing(self):
        self.status = Machine.STATUS_WORKING

    def working_piece_to_finished(self):
        self.instance.status = Machine.STATUS_CHANGING_PIECE
        print("Notificar finish")
        update_order_status(self.working_piece["piece_id"], "Manufactured")

    def add_piece_to_queue(self, piece):
        self.queue.append(piece)
        print("Adding piece to queue: {}".format(piece["piece_id"]))
        self.queue_not_empty_event.set()

    def remove_pieces_from_queue(self, pieces):
        for piece in pieces:
            if piece["status"] == "Queued":
                self.queue.remove(piece)
                update_order_status(self.working_piece["piece_id"], "Cancelled")
