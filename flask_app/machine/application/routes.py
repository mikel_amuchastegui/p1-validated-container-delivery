import traceback

from flask import abort
from flask import current_app as app
from flask import jsonify, request

from . import my_machine

from werkzeug.exceptions import (  # isort:skip
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
)


@app.route("/machine/manufacture", methods=["POST"])
def manufacture():
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json
    new_piece = None
    try:
        new_piece = {
            "piece_id": int(content["piece_id"]),
            "order_id": int(content["order_id"]),
            "status": "Created",
        }
        my_machine.add_piece_to_queue(new_piece)
    except KeyError:
        abort(BadRequest.code)
    response = jsonify(new_piece)
    return response


@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    return "OK"


@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
