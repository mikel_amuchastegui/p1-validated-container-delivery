import json

import pytest

from application import create_app, my_machine


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    with app.app_context():
        yield app
        my_machine.stop()
