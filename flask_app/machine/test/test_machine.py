import json

import pytest


def test_manufacture_unssuported_type(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/machine/manufacture",
        data=json.dumps({"piece_id": 1, "order_id": 1}),
        content_type="application/xml",
    )
    assert resp.status_code == 415


def test_manufacture(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/machine/manufacture",
        data=json.dumps({"piece_id": 1, "order_id": 1}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["piece_id"] == 1
    assert data["order_id"] == 1
    assert data["status"] == "Created"


def test_manufacture_incorrect_json_keys(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/machine/manufacture",
        data=json.dumps({"pieceId": 1, "order_id": 1}),
        content_type="application/json",
    )
    assert resp.status_code == 400
