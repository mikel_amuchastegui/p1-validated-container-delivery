import datetime
import json

import pytest

from application.businessLogic import BusinessLogic
from application.EventPublisher import EventPublisher
from application.models import Order


def test_create_order(test_app, monkeypatch):
    def mock_update_sagas(sagas_id, status):
        pass

    def mock_create_sagas(order_id, sagas):
        return {"id": 1, "status": sagas.status, "order_id": order_id}

    def mock_create_order(client_id, number_of_pieces):
        return {
            "id": 1,
            "number_of_pieces": number_of_pieces,
            "client_id": client_id,
            "delivery_id": None,
            "status": "Created",
            "pieces_manufactured": 0,
            "creation_date": datetime.datetime.now(),
            "timestamp": datetime.datetime.now(),
            "update_date": datetime.datetime.now(),
        }

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "create_order", mock_create_order)
    monkeypatch.setattr(BusinessLogic, "create_sagas", mock_create_sagas)
    monkeypatch.delattr("application.saga_order.Saga.on_event")
    monkeypatch.setattr(BusinessLogic, "update_sagas", mock_update_sagas)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.post(
        "/order",
        data=json.dumps(
            {"client_id": "mikel", "number_of_pieces": 10, "address": "48"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["client_id"] == "mikel"
    assert data["status"] == "Created"
    assert data["delivery_id"] == None
    assert data["number_of_pieces"] == 10


def test_view_order(test_app, monkeypatch):
    def mock_get_all_order():
        return [
            Order(number_of_pieces=10, client_id="mikel", status="Created"),
            Order(number_of_pieces=5, client_id="jon", status="Finished"),
        ]

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "get_all_order", mock_get_all_order)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.get("/order", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert data[0]["client_id"] == "mikel"
    assert data[0]["status"] == "Created"
    assert data[1]["client_id"] == "jon"
    assert data[1]["status"] == "Finished"


def test_update_order(test_app, monkeypatch):
    def mock_update_order(order_id, status):
        return {"client_id": "urtzi", "id": order_id, "status": status}

    def mock_send_data(self, data, routing_key):
        return

    monkeypatch.setattr(BusinessLogic, "update_order", mock_update_order)
    monkeypatch.setattr(EventPublisher, "send_data", mock_send_data)
    monkeypatch.delattr("application.EventHandler.EventHandler.run")

    client = test_app.test_client()
    resp = client.put(
        "/order/1",
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["client_id"] == "urtzi"
    assert data["status"] == "Finished"
