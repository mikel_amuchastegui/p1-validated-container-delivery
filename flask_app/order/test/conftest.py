import json

import pytest

from application import create_app, db, models
from application.EventPublisher import EventPublisher
from application.models import Order, Piece


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    app.config.from_object("application.config.TestingConfig")
    with app.app_context():
        yield app
        evPublish = EventPublisher("order_saga")
        evPublish.send_data(json.dumps({"stop": True}), "")


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.create_all(db.engine)
    yield db
    models.Base.metadata.drop_all(db.engine)


@pytest.fixture(scope="function")
def add_order():
    def _add_order(number_of_pieces, client_id, status):
        order = Order(
            number_of_pieces=number_of_pieces, client_id=client_id, status=status
        )
        db.session.add(order)
        db.session.commit()
        return order

    return _add_order


@pytest.fixture(scope="function")
def add_piece():
    def _add_piece(order_id, status):
        piece = Piece(order_id=order_id, status=status)
        db.session.add(piece)
        db.session.commit()
        return piece

    return _add_piece
