import json

import pytest


def test_create_order_unssuported_type(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/order",
        data=json.dumps(
            {"client_id": "mikel", "number_of_pieces": 10, "address": "48"}
        ),
        content_type="application/xml",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 415


def test_create_order(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/order",
        data=json.dumps(
            {"client_id": "mikel", "number_of_pieces": 10, "address": "48"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["client_id"] == "mikel"
    assert data["status"] == "Created"
    assert data["delivery_id"] == None
    assert data["number_of_pieces"] == 10


def test_view_orders(test_app, add_order, test_database):
    add_order(10, "mikel", "Created")
    add_order(5, "jon", "Finished")
    client = test_app.test_client()
    resp = client.get("/order", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert data[0]["client_id"] == "mikel"
    assert data[0]["status"] == "Created"
    assert data[1]["client_id"] == "jon"
    assert data[1]["status"] == "Finished"


def test_view_order_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/order/999999", content_type="application/json")
    assert resp.status_code == 404


def test_view_order(test_app, add_order, test_database):
    order = add_order(20, "xabi", "Created")
    client = test_app.test_client()
    resp = client.get("/order/" + str(order.id), content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["client_id"] == "xabi"
    assert data["status"] == "Created"


def test_update_order_unssuported_type(test_app, add_order, test_database):
    order = add_order(20, "joseba", "Created")
    client = test_app.test_client()
    resp = client.put(
        "/order/" + str(order.id),
        data=json.dumps({"status": "Finished"}),
        content_type="application/xml",
    )
    assert resp.status_code == 415


def test_update_order(test_app, add_order, test_database):
    order = add_order(10, "urtzi", "Created")
    client = test_app.test_client()
    resp = client.put(
        "/order/" + str(order.id),
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["client_id"] == "urtzi"
    assert data["status"] == "Finished"


def test_update_order_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.put(
        "/order/9999999",
        data=json.dumps({"status": "Finished"}),
        content_type="application/json",
    )
    assert resp.status_code == 400


def test_view_pieces(test_app, test_database, add_piece):
    add_piece(1, "Created")
    add_piece(2, "Manufacturing")
    add_piece(3, "Manufactured")
    client = test_app.test_client()
    resp = client.get("/piece", content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 3
    assert data[0]["order_id"] == 1
    assert data[0]["status"] == "Created"
    assert data[1]["order_id"] == 2
    assert data[1]["status"] == "Manufacturing"
    assert data[2]["order_id"] == 3
    assert data[2]["status"] == "Manufactured"


def test_view_piece(test_app, test_database, add_piece):
    piece = add_piece(1, "Created")
    client = test_app.test_client()
    resp = client.get("/piece/" + str(piece.id), content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["order_id"] == 1
    assert data["status"] == "Created"


def test_view_piece_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/piece/999999", content_type="application/json")
    assert resp.status_code == 404
