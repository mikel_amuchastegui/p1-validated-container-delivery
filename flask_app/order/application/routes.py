import traceback

from flask import abort
from flask import current_app as app
from flask import jsonify, request

from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher
from .models import Order, Piece
from .saga_order import Saga

from werkzeug.exceptions import (  # isort:skip
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
)

eventPublisher = EventPublisher("order")


@app.route("/order", methods=["POST"])
def create_order():
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)

    content = request.json

    new_order_dict = BusinessLogic.create_order(
        content["client_id"], content["number_of_pieces"]
    )

    order = Order(
        id=new_order_dict["id"],
        number_of_pieces=new_order_dict["number_of_pieces"],
        client_id=new_order_dict["client_id"],
        status=Order.STATUS_CREATED,
        delivery_id=new_order_dict["delivery_id"],
        pieces_manufactured=new_order_dict["pieces_manufactured"],
        creation_date=new_order_dict["creation_date"],
        timestamp=new_order_dict["timestamp"],
        update_date=new_order_dict["update_date"],
    )

    Saga(order, content["address"], eventPublisher)

    response = jsonify(new_order_dict)
    return response


@app.route("/order", methods=["GET"])
@app.route("/orders", methods=["GET"])
def view_orders():
    orders = BusinessLogic.get_all_order()
    print("GET All Orders.")
    response = jsonify(Order.list_as_dict(orders))
    return response


@app.route("/order/<int:id>", methods=["GET"])
def view_order(id):
    order = BusinessLogic.get_order(id)
    if order is None:
        abort(NotFound.code)
    print("GET Order {}: {}".format(id, order))
    response = jsonify(order.as_dict())
    return response


@app.route("/order/<int:id>", methods=["PUT"])
def update_order(id):
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)
    content = request.json
    status = content["status"]
    order_dict = BusinessLogic.update_order(id, status)
    if order_dict is None:
        eventPublisher.send_data(data="PUT_ERROR", routing_key="")
        abort(BadRequest.code)
    else:
        eventPublisher.send_data(data="PUT " + str(order_dict), routing_key="")
        response = jsonify(order_dict)
    return response


@app.route("/piece/<int:id>", methods=["PUT"])
def update_piece(id):
    if request.headers["Content-Type"] != "application/json":
        abort(UnsupportedMediaType.code)

    content = request.json
    status = content["status"]
    piece_dict = BusinessLogic.update_piece(id, status)
    if piece_dict is None:
        eventPublisher.send_data(data="Piece_PUT_ERROR", routing_key="")
        abort(BadRequest.code)
    else:
        response = jsonify(piece_dict)
    return response


@app.route("/piece", methods=["GET"])
@app.route("/pieces", methods=["GET"])
def view_pieces():
    pieces = BusinessLogic.get_all_pieces()
    response = jsonify(Piece.list_as_dict(pieces))
    return response


@app.route("/piece/<int:piece_ref>", methods=["GET"])
def view_piece(piece_ref):
    piece = BusinessLogic.get_piece(piece_ref)
    if piece is None:
        abort(NotFound.code)
    response = jsonify(piece.as_dict())
    return response


@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    return "OK"


@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
