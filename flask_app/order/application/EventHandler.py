#!/usr/bin/env python

import json
from os import environ
from threading import Thread

import pika


class EventHandler(Thread):
    def __init__(self, exchange, routing_key, type, saga, app):
        print("ORDER sagas event handler")
        Thread.__init__(self)
        rabbitmq_ip = environ.get("RABBITMQ_IP")
        pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
        self.connection = pika.BlockingConnection(pika_conn_param)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange=exchange, exchange_type="fanout", durable=True
        )
        result = self.channel.queue_declare(queue="", durable=True)
        self.channel.queue_bind(
            exchange=exchange, queue=result.method.queue, routing_key=routing_key
        )
        self.saga = saga
        self.type = type
        self.channel.basic_consume(
            queue=result.method.queue, auto_ack=False, on_message_callback=self.callback
        )
        self.app = app
        self.start()

    def run(self):
        print(" [*] Waiting for messages. To exit press CTRL+C")
        with self.app.app_context():
            self.channel.start_consuming()

    def callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body)
        jsonMsg = json.loads(body)
        if "stop" in jsonMsg and jsonMsg["stop"] == True:
            self.channel.stop_consuming()
        else:
            self.saga.on_event(jsonMsg["event"])
            ch.basic_ack(delivery_tag=method.delivery_tag)
