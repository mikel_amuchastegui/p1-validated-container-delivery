from . import db
from .api_client import update_delivery_status
from .models import Order, Piece, Sagas


class BusinessLogic:
    @staticmethod
    def create_order(client_id, number_of_pieces):
        try:
            new_order = Order(
                number_of_pieces=number_of_pieces,
                client_id=client_id,
                status=Order.STATUS_CREATED,
            )

            db.session.add(new_order)
            db.session.commit()
            new_order_dict = new_order.as_dict()
            db.session.close()
            return new_order_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def get_all_order():
        print("GET All Orders.")
        orders = db.session.query(Order).all()
        db.session.close()
        return orders

    @staticmethod
    def get_order(order_id):
        order = db.session.query(Order).get(order_id)
        if not order:
            db.session.close()
            return None
        db.session.close()
        return order

    @staticmethod
    def update_order(order_id, status):
        order = db.session.query(Order).get(order_id)
        if not order:
            return None
        order.status = status
        db.session.commit()
        order_dict = order.as_dict()
        db.session.close()
        return order_dict

    @staticmethod
    def create_piece(order_id):
        try:
            piece = Piece()
            order = BusinessLogic.get_order(order_id)
            piece.order = order
            db.session.add(piece)
            db.session.commit()
            piece_dict = piece.as_dict()
            db.session.close()
            return piece_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def get_all_pieces():
        orders = db.session.query(Piece).all()
        db.session.close()
        return orders

    @staticmethod
    def get_piece(piece_id):
        piece = db.session.query(Piece).get(piece_id)
        if not piece:
            db.session.close()
            return None
        db.session.close()
        return piece

    @staticmethod
    def update_piece(piece_id, status):
        try:
            piece = db.session.query(Piece).get(piece_id)
            piece.status = status
            order = db.session.query(Order).get(piece.order_id)
            order.pieces_manufactured += 1
            db.session.commit()
            if order.pieces_manufactured == order.number_of_pieces:
                update_delivery_status(order.delivery_id, "Finished")
            piece_dict = piece.as_dict()
            db.session.close()
            return piece_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def update_delivery_id(order_id, delivery_id):
        try:
            order = db.session.query(Order).get(order_id)
            order.delivery_id = delivery_id
            db.session.commit()
            db.session.close()
        except KeyError:
            db.session.rollback()
            db.session.close()

    @staticmethod
    def create_sagas(order_id, sagas):
        try:
            order = db.session.query(Order).get(order_id)
            sagas.order = order
            db.session.add(sagas)
            db.session.commit()
            sagas_dict = sagas.as_dict()
            db.session.close()
            return sagas_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    @staticmethod
    def update_sagas(sagas_id, status):
        try:
            sagas = db.session.query(Sagas).get(sagas_id)
            sagas.status = status
            db.session.commit()
            db.session.close()
        except KeyError:
            db.session.rollback()
            db.session.close()
