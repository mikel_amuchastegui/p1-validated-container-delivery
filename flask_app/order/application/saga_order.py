from flask import current_app

from .businessLogic import BusinessLogic
from .EventHandler import EventHandler
from .models import Sagas
from .saga_states import *


class Saga(object):
    def on_event(self, event):
        # The next state will be the result of the on_event function.
        self.state = self.state.on_event(event)

    def __init__(self, order, address, eventPublisher):
        """ Initialize the components. """

        # Start with a default state.
        EventHandler(
            exchange="order_saga",
            routing_key="",
            type="saga",
            saga=self,
            app=current_app._get_current_object(),
        )
        sagas = Sagas(order_id=order.id, status=Sagas.STATUS_ORDER_ORDERED)
        sagas_dict = BusinessLogic.create_sagas(order.id, sagas)
        self.state = OrderOrderedState(order, address, sagas_dict["id"], eventPublisher)
