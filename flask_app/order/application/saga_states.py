import json

from .api_client import create_piece, get_delivery_id, update_delivery_status
from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher
from .models import Order
from .state import State

eventPublisherPayment = EventPublisher("payment_saga")
eventPublisherDelivery = EventPublisher("delivery_saga")


class OrderOrderedState(State):
    """
    The state which indicates that there are limited device capabilities.
    """

    def __init__(self, order, address, sagas_id, eventPublisher):
        print("Order Sagas starting")
        print("Send to payment info from order sagas")
        eventPublisherPayment.send_data(
            json.dumps(
                {
                    "exchange_response": "order_saga",
                    "balance": order.number_of_pieces * 10,
                    "client_id": order.client_id,
                }
            ),
            "",
        )
        eventPublisher.send_data(
            "SAGAS: Sagas in OrderOrderedState, "
            + "starting Order with ID: "
            + str(order.id),
            "",
        )
        BusinessLogic.update_sagas(sagas_id, self.__repr__())
        self.order = order
        self.address = address
        self.sagas_id = sagas_id
        self.eventPublisher = eventPublisher

    def on_event(self, event):
        if event == "payment_done":
            self.eventPublisher.send_data(
                "SAGAS: Sagas in "
                + "OrderOrderedState, "
                + "changing state of order with ID: "
                + str(self.order.id)
                + " to OrderPendingDeliveryState",
                "",
            )
            return OrderPendingDeliveryState(
                self.order, self.address, self.sagas_id, self.eventPublisher
            )
        else:
            self.eventPublisher.send_data(
                "SAGAS: Sagas in OrderOrderedState, "
                + "changing state of order with ID: "
                + str(self.order.id)
                + " to OrderRejectedState. Reason: NOT ENOUGH MONEY",
                "",
            )
            return OrderRejectedState(self.order, self.sagas_id, self.eventPublisher)
        return self


class OrderPendingDeliveryState(State):
    """
    The state which indicates that there are no limitations on device
    capabilities.
    """

    def __init__(self, order, address, sagas_id, eventPublisher):
        print("Starting delivery")
        eventPublisherDelivery.send_data(
            json.dumps(
                {
                    "exchange_response": "order_saga",
                    "address": address,
                    "client_id": order.client_id,
                    "order_id": order.id,
                }
            ),
            "",
        )
        BusinessLogic.update_sagas(sagas_id, self.__repr__())
        eventPublisher.send_data(
            "SAGAS: Sagas in "
            + "OrderPendingDeliveryState, "
            + "starting delivery in Order with ID: "
            + str(order.id),
            "",
        )
        self.order = order
        self.sagas_id = sagas_id
        self.eventPublisher = eventPublisher

    def on_event(self, event):
        print("order_id: " + str(self.order.id))
        delivery_dict = get_delivery_id(self.order.id)
        self.order.delivery_id = delivery_dict["id"]
        BusinessLogic.update_delivery_id(self.order.id, self.order.delivery_id)
        if event == "delivery_done":
            self.eventPublisher.send_data(
                "SAGAS: Sagas in "
                + "OrderPendingDeliveryState,"
                + " changing state of "
                + "order with ID: "
                + str(self.order.id)
                + " to OrderApprovedState",
                "",
            )
            return OrderApprovedState(self.order, self.sagas_id, self.eventPublisher)
        else:
            self.eventPublisher.send_data(
                "SAGAS: Sagas in "
                + "OrderPendingDeliveryState, "
                + "changing state of order with ID: "
                + str(self.order.id)
                + " to OrderRefoundState. "
                + "Reason: ZIP code incorrect",
                "",
            )
            return OrderRefoundState(self.order, self.sagas_id, self.eventPublisher)
        return self


class OrderApprovedState(State):
    """
    The state which indicates that there are no limitations on device
    capabilities.
    """

    def __init__(self, order, sagas_id, eventPublisher):
        BusinessLogic.update_sagas(sagas_id, self.__repr__())
        eventPublisher.send_data(
            "SAGAS: Sagas in OrderApprovedState, "
            + "starting manufacturing of pieces in "
            + "Order with ID: "
            + str(order.id),
            "",
        )
        for i in range(order.number_of_pieces):
            piece_dict = BusinessLogic.create_piece(order.id)
            eventPublisher.send_data(
                "SAGAS: Sagas in OrderApprovedState, starting manufacturing"
                + " of piece ID: "
                + str(piece_dict["id"])
                + " in Order with ID: "
                + str(order.id),
                "",
            )
            print(piece_dict)
            create_piece(
                order_id=order.id,
                piece_id=piece_dict["id"],
                status=piece_dict["status"],
            )

        self.sagas_id = sagas_id
        self.eventPublisher = eventPublisher
        eventPublisher.send_data(
            "SAGAS: Sagas in OrderApprovedState, "
            + "manufacturing of pieces finished "
            + "and Order with ID: "
            + str(order.id)
            + " in delivery state",
            "",
        )


class OrderRefoundState(State):
    """
    The state which indicates that there are no limitations on device
    capabilities.
    """

    def __init__(self, order, sagas_id, eventPublisher):
        eventPublisherPayment.send_data(
            json.dumps(
                {
                    "exchange_response": "order_saga",
                    "balance": -(order.number_of_pieces * 10),
                    "client_id": order.client_id,
                }
            ),
            "",
        )
        BusinessLogic.update_sagas(sagas_id, self.__repr__())
        eventPublisher.send_data(
            "SAGAS: Sagas in OrderRefoundState, "
            + "starting refunding of money to client "
            + str(order.client_id)
            + " in Order with ID: "
            + str(order.id),
            "",
        )
        self.order = order
        self.sagas_id = sagas_id
        self.eventPublisher = eventPublisher

    def on_event(self, event):
        if event == "payment_done":
            self.eventPublisher.send_data(
                "SAGAS: Sagas in OrderRefoundState, "
                + "successful refunding of money to client "
                + str(self.order.client_id)
                + " in Order with ID: "
                + str(self.order.id),
                "",
            )
            return OrderRejectedState(self.order, self.sagas_id, self.eventPublisher)
        return self


class OrderRejectedState(State):
    """
    The state which indicates that there are no limitations on device
    capabilities.
    """

    def __init__(self, order, sagas_id, eventPublisher):
        BusinessLogic.update_order(order.id, Order.STATUS_REJECTED)
        BusinessLogic.update_sagas(sagas_id, self.__repr__())
        eventPublisher.send_data(
            "SAGAS: Sagas in OrderRejectedState, "
            + "rejecting Order with ID: "
            + str(order.id)
            + " and delivery with ID: "
            + str(order.delivery_id)
            + " in case it was created",
            "",
        )
        update_delivery_status(order.delivery_id, "Rejected")
        self.sagas_id = sagas_id
        self.eventPublisher = eventPublisher
