#!/usr/bin/env python
import time
from os import environ

import pika
from pika import exceptions


class EventPublisher:
    def __init__(self, exchange):
        self.exchange = exchange
        print("Payment")
        rabbitmq_ip = environ.get("RABBITMQ_IP")
        pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
        self.connection = pika.BlockingConnection(pika_conn_param)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange=exchange, exchange_type="fanout", durable=True
        )
        self.exchange = exchange

    def open_connection(self, exchange):
        while True:
            try:
                print("Payment")
                rabbitmq_ip = environ.get("RABBITMQ_IP")
                pika_conn_param = pika.ConnectionParameters(rabbitmq_ip)
                self.connection = pika.BlockingConnection(pika_conn_param)
                self.channel = self.connection.channel()
                self.channel.exchange_declare(
                    exchange=exchange, exchange_type="fanout", durable=True
                )
                self.exchange = exchange
            except exceptions.AMQPConnectionError:
                time.sleep(5)
                continue
            break

    def send_data(self, data, routing_key):
        data = str(data)
        self.open_connection(self.exchange)
        self.channel.basic_publish(
            exchange=self.exchange,
            routing_key=routing_key,
            body=data,
            properties=pika.BasicProperties(delivery_mode=2,),
        )

        print(" [x] Sent " + data)
        self.close()

    def close(self):
        self.connection.close()
