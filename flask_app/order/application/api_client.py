import json
from os import environ

import requests

headers = {"Content-type": "application/json"}


def request_payment(balance, client_id):
    payment_data = {"balance": balance}
    payment_response = requests.put(
        url=environ.get("HAPROXY_IP") + "/payment/" + str(client_id),
        data=json.dumps(payment_data),
        headers=headers,
    )
    return payment_response


def create_piece(order_id, piece_id, status):
    machine_data = {"order_id": order_id, "piece_id": piece_id, "status": status}
    machine_response = requests.post(
        url=environ.get("HAPROXY_IP") + "/machine/manufacture",
        data=json.dumps(machine_data),
        headers=headers,
    )
    return machine_response


def create_delivery(order_id, user_id, address):
    delivery_data = {"address": address, "user_id": user_id, "order_id": order_id}

    delivery_response = requests.post(
        url=environ.get("HAPROXY_IP") + "/delivery",
        data=json.dumps(delivery_data),
        headers=headers,
    )
    return delivery_response


def update_delivery_status(delivery_id, status):
    delivery_data = {"status": status}
    delivery_response = requests.put(
        url=environ.get("HAPROXY_IP") + "/delivery/" + str(delivery_id),
        data=json.dumps(delivery_data),
        headers=headers,
    )
    return delivery_response


def get_delivery_id(order_id):
    print("order_id: " + str(order_id))
    delivery_response = requests.get(
        url=environ.get("HAPROXY_IP") + "/delivery/order/" + str(order_id)
    )

    return delivery_response.json()
